package qa.excersises;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AngleTest {

    @BeforeEach
    void setUp() throws Exception{
    }

    @Test
    public void test(){
        Angle angleNum = new Angle(10);
        assertEquals(10, angleNum.getDegrees(), "They are not equal");
        //fail("not yet Implemented");
    }

    @Test
    public void angle_test_negative(){
        Angle angleNum = new Angle(-10);
        assertEquals(350, angleNum.getDegrees(), "They are not equal");
    }

    @Test
    public void angle_test_less_than__negative_360(){
        Angle angleNum = new Angle(-370);
        assertEquals(350, angleNum.getDegrees(), "They are not equal");
    }

    @Test
    public void angle_test_greater_than_360(){
        Angle angleNum = new Angle(390);
        assertEquals(30, angleNum.getDegrees(), "They are not equal");
    }
}